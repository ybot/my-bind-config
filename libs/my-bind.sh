#!/bin/bash
. ${MY_LIB_COMMON:-/lib/mygw/common.sh}

: ${DHCP_ZONE_CONF:=/etc/dhcp/dhcpd6-ddns-zones.conf}
: ${BIND_INCLUDE_FILE:=/etc/bind/named.conf.local}
: ${BIND_OPTIONS_FILE:=/etc/bind/named.conf.options}
: ${ZONE_CONFIG_PATH:=/etc/bind/my-zones}
: ${ZONE_PATH:=/var/lib/bind}
: ${DNSSEC_KEY_PATH:=/etc/bind/keys-dnssec}
: ${SIG0_KEY_PATH:=/etc/bind/keys-sig0}
: ${DDNS_KEY_PATH:="/etc/bind/keys-ddns"}
: ${XFER_KEY_PATH:="/etc/bind/keys-ddns"}
: ${MY_DOH_PORT:=8443}
: ${MY_DOT_PORT:=853}


function get_zone {
	local _type=${1}
	local _domain=${2%.}.
	local _name

	case ${_type} in
		my_zone)
			_name=$(dig +noall +authority +answer SOA ${_domain} | awk '{ print $1 }')
		;;
		my_master)
			_name=$(dig +noall +authority +answer SOA ${_domain} | awk '{ print $5 }')
		;;
		parent_zone)
			_name=$(dig +noall +authority +answer SOA ${_domain#*.} | awk '{ print $1 }')
		;;
		parent_master)
			_name=$(dig +noall +authority +answer SOA ${_domain#*.} | awk '{ print $5 }')
		;;
	esac

	[ -z "${_name}" ] && return 1
	echo "${_name}"
}

function get_sig0_pubkey {
	local _domain=${1}
	local _ip=${2}
	local _key
	_key=$(dig +short KEY ${_domain} @${_ip})
	[ -z "${_key}" ] && return 1
	echo ${_key}
}

function get_key {
	local _type=$1
	local _domain=${2%.}
	local _key

	case ${_type} in
		ddnsname)
			_key="ddns-${_domain}"
			echo ${_key}
		;;

		ddnsfile)
			_key="${DDNS_KEY_PATH}/ddns-${_domain}.key"
			[ ! -e "${_key}" ] && return 1
			echo ${_key}
		;;
	esac

}

function ptr6zone {
	local _zone=$1
	local _ip=${_zone%%/*}
	local _prefix=${_zone#*/}
	local _ptr
	[ "${_prefix}" == "${_ip}" ] && _prefix=128
	[[ "${_prefix}" =~ ^[0-9]+$ ]] || debug "preifx /${_prefix} is not valid" || return 1
	_ptr=$(sipcalc -r ${_ip} | grep 'ip6\.arpa\.' |  sed "s/.*\([0-9a-f.]\{$((${_prefix}/2))\}ip6\.arpa\)\./\1/g") || debug "failed to detect ptr zone for ${_zone}" || return 1
	[ -z "${_ptr}" ] && debug "failed to find ptr zone for ${_zone}" && return 1
	echo "${_ptr}"
}

function ptr4zone {
    local _zone=$1
    local _ip=${_zone%%/*}
    local _prefix=${_zone#*/}
    IFS='./' read -ra IPv4 <<< "${_ip}"

    [ "${_prefix}" == "${_ip}" ] && _prefix=32
    case "${_prefix}" in
        32)
            printf '%d.%d.%d.%d.in-addr.arpa\n' $((10#${IPv4[3]})) $((10#${IPv4[2]})) $((10#${IPv4[1]})) $((10#${IPv4[0]}))
            ;;
        24)
            printf '%d.%d.%d.in-addr.arpa\n' $((10#${IPv4[2]})) $((10#${IPv4[1]})) $((10#${IPv4[0]}))
            ;;
        16)
            printf '%d.%d.in-addr.arpa\n' $((10#${IPv4[1]})) $((10#${IPv4[0]}))
            ;;
        8)
            printf '%d.in-addr.arpa\n' $((10#${IPv4[0]}))
            ;;
        *)
            debug "incompatible prefix ${_prefix}"
            return 1
            ;;
    esac
}

